module CsvNormalizer
  # Makes all names uppercase
  class NameNormalizer
    def self.call(name)
      name.mb_chars.upcase
    end
  end
end