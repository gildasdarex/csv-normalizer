require 'spec_helper'

RSpec.describe CsvNormalizer::ZipCodeNormalizer do
  it 'normalizes a zip code' do
    expect(
      described_class.call('123')
    ).to eq('00123')
  end
end
