require 'spec_helper'

RSpec.describe CsvNormalizer::TextNormalizer do
  it 'converts an address to utf-8 encoding' do
    expect(
      described_class.call('converts an address to utf-8 encoding').encoding.name
    ).to eq('UTF-8')
  end
end
