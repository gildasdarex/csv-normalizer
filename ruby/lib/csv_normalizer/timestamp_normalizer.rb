require 'active_support/all'

module CsvNormalizer
  # convert the timestamp from us/pacific to us/eastern and formats it into iso8601
  class TimestampNormalizer
    def self.call(timestamp)
      Time.zone = 'Hawaii'

      Time.zone
          .strptime(timestamp, '%m/%d/%y %H:%M:%S')
          .in_time_zone('America/New_York')
          .iso8601
    end
  end
end