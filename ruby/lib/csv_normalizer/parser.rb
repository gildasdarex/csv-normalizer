require 'smarter_csv'
require 'csv'
require 'logging'

module CsvNormalizer
  class Parser
    attr_reader :path

    def self.run(path)
      new(path).run
    end
  
    def initialize(path)
      @path = path
      @logger = Logging.logger(STDOUT)
      @logger.level = :debug
    end
  
    def run
      @logger.debug "Start normalization of file #{@path} "
      return unless can_parse_file?
      results = []

      rows.each_with_index do |row, index|
        begin
          results.push(parse_row(Hash.new('').merge(row)))
        rescue UndefinedConversionError
          @logger.debug "Parsing error on line #{index}"
        end
      end

      normalize_file = build_file(results)
      @logger.debug "Ended normalization of file #{@path} "
      @logger.debug "Normalized file location is #{normalize_file} "
    end

    private

    def rows
      SmarterCSV.process(
        path,
        col_sep: ',', row_sep: :auto, headers: true, encoding: 'utf-8', force_utf8: true
      )
    end

    def parse_row(row)
      results = {}
      results['Timestamp']     = TimestampNormalizer.call(row[:timestamp])
      results['Address']       = TextNormalizer.call(row[:address])
      results['ZIP']           = ZipCodeNormalizer.call(row[:zip])
      results['FullName']      = NameNormalizer.call(row[:fullname])
      results['FooDuration']   = DurationNormalizer.call(row[:fooduration])
      results['BarDuration']   = DurationNormalizer.call(row[:barduration])
      results['TotalDuration'] = (results['FooDuration'] || 0) + (results['BarDuration'] || 0)
      results['Notes']         = TextNormalizer.call(row[:notes])

      results
    end

    def build_file(data)
      normalize_file = "#{File.dirname(@path)}/#{File.basename(@path, '.csv')}_normalized.csv"
      CSV.open(normalize_file, 'wb') do |csv|
        csv << data.first.keys
        data.each do |row|
          csv << row.values
        end
      end
      normalize_file
    end

    def can_parse_file?()
      unless(File.exist?(@path))
        @logger.debug " File #{@path} not found "
        return false
      end

      if(File.zero?(@path))
        @logger.debug " File #{@path} is empty "
        return false
      end

      true
    end

  end
end