require 'spec_helper'

RSpec.describe CsvNormalizer::DurationNormalizer do
  it 'normalizes a duration' do
    expect(
      described_class.call('10:10:10.123').to_s
    ).to eq('36610.123')
  end
end
