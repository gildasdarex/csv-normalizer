require 'spec_helper'

RSpec.describe CsvNormalizer::TimestampNormalizer do
  it 'normalizes a timestamp' do
    expect(
      described_class.call('4/1/11 11:00:00 AM')
    ).to eq('2011-04-01T17:00:00-04:00')
  end
end
