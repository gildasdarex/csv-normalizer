module CsvNormalizer
  # converts the text invalid characteres into utf8
  class TextNormalizer
    def self.call(text)
      text.encode('UTF-8')
    end
  end
end