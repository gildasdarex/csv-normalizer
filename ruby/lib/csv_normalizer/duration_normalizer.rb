# require 'active_support/all'

module CsvNormalizer
  # Remove milliseconds and add them to the seconds
  class DurationNormalizer
    def self.call(duration)
      hour, min, sec = duration.split(':')

      hour.to_i * 3600 + min.to_i * 60 + sec.to_i + (sec.to_f - sec.to_i)
    end

    def self.to_sec(value, type)
      value = value.to_i

      case type
      when 'hour'
        value * 3600
      when 'min'
        value * 60
      when 'usec'
        value/1000000.0
      end
    end
  end
end