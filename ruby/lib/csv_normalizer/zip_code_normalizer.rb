module CsvNormalizer
  # Offset zip code with 0s, to make it a 5 digits value
  class ZipCodeNormalizer
    def self.call(zip_code)
      offset_zip_code = "00000#{zip_code}"

      offset_zip_code[(offset_zip_code.size - 5)..-1]
    end
  end
end