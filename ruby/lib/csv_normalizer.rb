require "csv_normalizer/version"
require 'csv_normalizer/parser'
require 'csv_normalizer/timestamp_normalizer'
require 'csv_normalizer/zip_code_normalizer'
require 'csv_normalizer/name_normalizer'
require 'csv_normalizer/duration_normalizer'
require 'csv_normalizer/text_normalizer'

module CsvNormalizer
  class Error < StandardError; end
  # Your code goes here...
end