require 'spec_helper'

RSpec.describe CsvNormalizer::NameNormalizer do
  it 'normalizes a name' do
    expect(
      described_class.call('john doe')
    ).to eq('JOHN DOE')
  end
end
