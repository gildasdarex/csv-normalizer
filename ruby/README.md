# CsvNormalizer
Basic CsvNormalizer

## Requirements

- Ruby 2.3.7 or higher
- Ubuntu
- Bundler

## Install Ruby

```
 $ gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
 $ curl -sSL https://get.rvm.io | bash -s stable --ruby
 $ source /usr/local/rvm/scripts/rvm
 $ ruby -v 
```

## How to build ?
```
 $ git clone https://gitlab.com/gildasdarex/csv-normalizer
 $ cd csv-normalizer
 $ git checkout dev
 $ cd ruby
 $ gem install bundler -v 2.0.1
 $ bundle install
 $ gem build csv_normalizer.gemspec
 $ gem install csv_normalizer-0.1.0.gem
```

## Usage
```
 $ csv_normalizer path_to_file.csv
```

## Run the test suite
```
 $ cd csv-normalizer/ruby
 $ bundle exec rspec .